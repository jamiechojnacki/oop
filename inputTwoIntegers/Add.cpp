#include "Add.hpp"
#include <iostream>

Add::Add(int inputLeftOperand, int inputRightOperand)
    : firstOperand(inputLeftOperand), secondOperand(inputRightOperand)
{
}

void Add::displaySum() const noexcept {
    std::cout << "sum: " << firstOperand + secondOperand << std::endl;
}