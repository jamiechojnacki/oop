#ifndef _ADD_HPP_
#define _ADD_HPP_

class Add {
    public:
    Add() = delete;
    Add(int, int);
    void displaySum() const noexcept;

    private:
    int firstOperand;
    int secondOperand;

};

#endif