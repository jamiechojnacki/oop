#ifndef _BASISPOINT_HPP_
#define _BASISPOINT_HPP_

class BasisPoint {
    public:
    BasisPoint() = delete;
    BasisPoint(int, int);

    void displaySum() const noexcept;

    private:
    int imaginaryPoint;
    int realPoint;
};

#endif