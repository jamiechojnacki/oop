#include "BasisPoint.hpp"
#include <iostream>
#include <string>

BasisPoint::BasisPoint(int imaginaryNumber, int realNumber) 
    : imaginaryPoint(imaginaryNumber), realPoint(realNumber)
{
}

void BasisPoint::displaySum() const noexcept {
    std::string prettyPrint = (this->imaginaryPoint < 0) ? " " : "+";
    std::cout << "sum :" << this->realPoint << prettyPrint << this->imaginaryPoint << "i" << std::endl;
}
