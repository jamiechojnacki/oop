#include <iostream>
#include "BasisPoint.hpp"

int main(int, char**) {
    int realNumberInput = 0;
    int imaginaryNumberInput = 0;
    std::cout << "input RE then IM" << std::endl;
    std::cin >> realNumberInput >> imaginaryNumberInput;

    BasisPoint surface(imaginaryNumberInput, realNumberInput);
    surface.displaySum();

}
